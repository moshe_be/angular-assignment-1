import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-left-side-barrel',
  templateUrl: './left-side-barrel.component.html',
  styleUrls: ['./left-side-barrel.component.scss']
})
export class LeftSideBarrelComponent implements OnInit {


  /**
   * Task 1: 
   * This variable should get it's data passed from app-component (there is a ready array there), currently there is mock data but it should be deleted or ignored.
   * assume the data in app-component will arrive from a server and you cannot create it here.
   * 
   * 
   * 
   * Task 2: 
   * Right now - the html only shows the item name. please make sure it *also* shows the date field in this formatting: 'MM - dd, - yyyy' - for example:
   * the date 25/12/2001 will be presented as '12 - 25, - 2001'
   * 
   * note - to do this you will need to use an angular pipe.
   */
  @Input() dataItems = [{
    name : 'default item', 
    date : new Date()
  }, {
    name : 'default item 2',
    date : new Date()
  }];


  constructor() { }

  ngOnInit() {
  }


  /**
   * Task 3 -
   * This is the function that runs on the button click in the UI.  when the button is clicked - 
   * it should pass data to app-component (this component's parent component) and run the function addItem() that is already there.
   */
  addItemClicked(){
    //TODO: make me work
  }

}
