import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  /**
   * Task 1 - this is the data that should be presented in our child component instead of the data currently present there.
   */
  dataToPass = [{
   name : 'Item A',
   date : new Date(1950, 10, 10)
  },{
    name : 'Item B',
    date : new Date(2019, 6,21)
  }, {
    name : 'Item C',
    date : new Date()
  }];


  addItem() {
    this.dataToPass.push({
      name : 'new item added!',
      date : new Date()
    })
  }


  /**
   * Task 4 - Advanced - Optional 
   * Create a second component - right-side-barrel
   * add a button on app-component that will read : 'Pass to right side'
   * 
   * clicking the button will take the upper most element in the left side barrel, remove it and pass it to the new right-side-barrel component.
   * 
   */
  onPassToRightSideClicked(){
    //TODO: this will be the functionn the button will run to pass the data.
  }

}
