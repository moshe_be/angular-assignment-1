import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LeftSideBarrelComponent } from './component-1/left-side-barrel.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftSideBarrelComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
